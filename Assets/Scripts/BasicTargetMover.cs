﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTargetMover : MonoBehaviour 
{
    public float spinSpeed;
    public float motionMagnitude;
    public bool doSpin;
    public bool doMotion;

    void Update()
    {
        //In the code below we used the Time.deltime, because we wanted to move the object at a constant speed.
        //And to make its movement smooth across all hardware types.

        if (doSpin)
            transform.Rotate(Vector3.up * spinSpeed * Time.deltaTime);
        if (doMotion)
            transform.Translate(Vector3.up * Mathf.Cos(Time.timeSinceLevelLoad) * motionMagnitude);

    }

}
