# Box Shooter Project

This project will be used to test the candidates in the position at Technomar.

## Introduction

This project use traditional first-person shooter controls to shoot a variety of targets. The targets will either increase or decrease your score or increase or decrease the timer. The game is over when you run out of time.

## Tasks

The project was developed using git to provide the control version. Always use the git to control your development steps. 

The time spected to develop this game is around 3 hours.

An example is provided in zip file, in the builds/windows directory. You can play and check the final result of the game.

First of all, create a new project called "Box Shooter" in the Unity 3D (try to use the version 5.5.1f1) and import the assets to be use in this project.

After this, we will create several elements in this project.

### Movable Targets

The objective in this task is create a box to serve as a target. 

Write a custom script and make public variables that show up in the Unity Editor. Write code that moves the gameObject through the transform component.

1. Create a Cube named `Target` with this values in transform Position(0,3,0);Rotation(0,0,0);Scale(2,2,2). Apply the `Target-Positive Material` in the Material folder.
2. Add a new script in this `gameObject` with name `BasicMover` and:
3.  Create a public variable called spinSpeed and provide a rotation in the `gameObject` with this parameter `Vector3.up * spinSpeed * Time.deltaTime`;
4.  Create a public variable called motionMagnitude and provide a Translation with this parameter `Vector3.up * Mathf.Cos(Time.timeSinceLevelLoad) * motionMagnitude`;
5.  Insert two public bool variables, called `doSpin` and `doMotion` to enable and disable the Spin and the Motion;
6.  Rename the Scrip to `BasicTargetMover` and solve the errors arrived from this change;
7. Explain (in portuguese if you prefer) why we use `Time.deltaTime`;
8. Remove your `BasicTargetMover` script and insert the `TargetMover` script and create a prefab and remove `Target` Object

### Player Setup

In this task, the objective is to elaborate the Scene with the cameras, players, projectil and shoot.

#### Controller

The objective here is create a gameObject for the player and attach the character controller component and explore the functionalities of controllable script.

1. Crate a `Capsule` shape to represent the player, in this position (0,1,0), and rename the gameObject to `Player` and define your tag to `Player`;
2. Add a component `Character Controller` this gameObject;
3. Insert the script `Controller` in this gameObject.

### Camera

The objective here is create a camera to follow the player, respond to the mouse event, set a skybox, apply image effects and insert a music.

1. Move the `Main Camera` to position (0,2,0) and make the camera to follow the player;
2. Insert the script `MouseLooker` as `Player` component;
3. Insert a skybox component in the `Main Camera` and select the `Skybox Material` pre-created in Material folder;
4. Insert a component `Contrast Enhance` and change the intensity to `1`;
5. Insert a component `Bloom` and change the `bloom intensity` to `2`;
6. Insert a `audio source` component and insert the `BackgroundMusic` and set the `volume` to `0.25` and the `pitch` to `1.2`;
7. Remove the player shadow;
8. Create a prefab of `Player` object.

### Projectile

The objective here is create a projectile, apply a rigid body, add a trail renderer and create a prefab.

1. Create a `Sphere` and rename to `Projectile` and apply `Projectile Material`and resize the sphere to `0.2` in all directions;
2. Add a `rigid body` component and remove the gravity;
3. Add a `trail renderer` component and insert the default material called `Default-Particle` and define the `Width` of `0.1` starting with `0.1` and end with `0.0`, modify the colors to use `#00FFDF` and select 4 points to decrease the alpha in the color;
4. Create a prefab.

### Shoot Projectiles

Our objectives will be to add a few targets to our scene and apply the shooter script so we can shoot projectiles and we will make sure the projectiles destroy themselves after a period of time, to keep our game running smoothly. 

1. Insert 3 targets from `Target` prefab in:
    * Position (0,6,10) and Spin;
    * Position (-8,6,10) and horizontal;
    * Position (8,6,10) and vertical.
2. Create a empty game object to hold all targets called `Targets` and reset your tranform component;
3. In the `Main Camera` game object, insert a new script called `Shooter`, set the Projectile with `Projectile` prefab, the power to `200` and insert `Laser` as Shoot SFX parameter;
4. Insert `TimeObjectDestructor` component in `Projectile` prefab and set the Time Out to `2`.
5. Why we need to do the last step in our project? (Can be anwser in portuguese).

## Game Interface

The main objective here is to provide a game engine, an UI to interact with the user and some improvements in the game.

### UI Setup

The objective here is to create a target in the UI, create the scoreboard and a timer display.

1. Create a canvas with name `MainCanvas`;
2. Insert in the canvas, a image to be the crosshair target in the middle of screen, rename to `Crosshair`, select `Crosshair` image and rescale to (144,144);
3. In the `MainCanvas` insert another image, to be the background of the scoreboard, and rename the image to `Scoreboard`, anchoring in the middle of canvas, and define the size with (160,50) and the position `Y` to 100 px. Set the image `InputFieldBackground` and change the color to `#00000064`;
4. In the `Scoreboard`, insert a `Text` and rename to `Score Text`. Change the text to `0` to hold the score and change the size of font to `40`, style to `Bold`, Alignment to `center` and `middle`, changes the horizontal and vertical overflow to `Overflow` and change the color to `#00FFDFFF`;
5. Create a new image called `GameOver Score Outline` as child of `Scoreboard`, change the image to `InputFieldBackground`, define the color as `00FFDFFF`, disable `Fill Center` and resize the image as the same size of `Scoreboard`, after this, disable the this game object;
6. Create a new text element in the `MainCanvas` and rename to `Timer Text`, align with the center and set you position to (0,160), the text set to `15.0`, change the size of font to `20`, style to `Bold`, Alignment to `center` and `middle`, changes the horizontal and vertical overflow to `Overflow` and change the color to `white`.

### Game Manager

 The objetives here is to create the game manager, add the target behavior and adjust rigid body collision detection.

1. Create a empty game object to be the `Game Manager`, and insert a `Game Manager` script with these values;
    * `Main Score Display` = `Score Text`
    * `Main Timer Display` = `Timer Text`
    * `Game Over Score Display` = `GameOver Score Outline`
    * `Music Audio Source` = `Main Camera`
2. In the `Target` prefab, insert the script `TargetBehavior`;
3. Define the tag `Projectile` in the Projectile prefab;
4. In the rigid body in `Projectile` prefab, define the `collision detection` to `Continuous`;
5. Why we made the last modification in the rigid body? (You can answer in Portuguese).

### Play Again
The objectives here is to create a 3d Text, apply collider and script and polish the game.

1. Create a 3D text to execute the play again buttom in (0,3,14) position. Rename them to `PlayAgain` and align to the `middle center`, write text `PLAY AGAIN`, set font style to `bold` and change the color to `#FFFFFF96`;
2. Insert a `box collider`  component in this text and change the size of z direction to `1`;
3. Insert the script called `PlayAgain` in this object;
4. Go to the `Game Manager` and define the value `Level1` to `Play Again Level To`;
5. Create a prefab of `PlayAgain`;
6. Insert 4 `PlayAgain` buttons, one for each side of the cube, where the player is contained;
7. Create an empty game object, rename them to `PlayAgain Buttons`, reset your coordinates and define all four buttons to be child of him;
8. In the `PlayAgaing Buttons`, disable them, and insert `Rotate` script as component, and change the variable `Way` to `around Y`, to provide a rotation in all buttons;
9. Go to the `Game Manager` and insert `PlayAgain Buttons` object in the `PlayAgain Buttons` variable.

### Particle and SFX

We will insert a set of particles and sound effects.

1. Create a `Particle System` game object, renamed to `Target-Positive-Explosion`, and reset the transform to transfer to center of the world;
2. Modify some values, as follow:
    * Target-Positive-Explosion:
        - `Duration` = 0.4;
        - `Looping` = false;
        - `Start Size` -> `Between two constants` ans set `0.2` to `0.5`;
        - `Gravity modifier` = 0.5;
    * Emmission:
        - `Rate over time` = 0;
        - Add a burst with 0.0 and 10 min and 10 max particles;
    * Shape:
        - `Shape` = Sphere;
    * Size of life time:
        - Take one shape as you prefer;
    * Renderer:
        - `Render Mode` = Mesh;
        - `Mesh` = cube;
        - `Material` = Target-Positive Material
3. Insert an `audio source` component and insert in the audio clip, the `PositiveHit`;
4. Insert the `Target object destructor` script as component.
5. Create a prefab of this game object;
6. Duplicate this prefab and create a negative and bonus version of `Target-Positive-Explosion`, modify only the material and the sound.

## Target and Spawn

In this step we will polite the game to create a set of prefabs to the targets and define how they will be spawned. Some animations will be made.

### Target Prefab setup

Create 4 types of target prefabs, these types are:
* Two types of positive targets;
* One type of negative target;
* One type of bonus target;

The base of all prefabs is the target prefab made in previus task.

First of all, update the base target insert a new script as component `Target Exit` to define a live time to stay instatiate in the scenario.

After this, create the prefabs. In the next section I will write the differences between the base prefab and the derivaded prefab.


### Positive Target 1

Component (Target Behavior)->`Score Amount` = 5;
Component (Target Behavior)->`Time Amount` = 0;
Component (Target Behavior)->`Explosion Prefab` = Target-Positive-Explosion;
Component (Target Mover)->`Motion State` = Horizontal;
Material -> Target-Positive Material

### Positive Target 2

Component (Target Behavior)->`Score Amount` = 5;
Component (Target Behavior)->`Time Amount` = 0;
Component (Target Behavior)->`Explosion Prefab` = Target-Positive-Explosion;
Component (Target Mover)->`Motion State` = Vertical;
Material -> Target-Positive Material

### Negative Target

Component (Target Behavior)->`Score Amount` = 0;
Component (Target Behavior)->`Time Amount` = -3;
Component (Target Behavior)->`Explosion Prefab` = Target-Negative-Explosion;
Component (Target Mover)->`Motion State` = Spin;
Material -> Target-Negative Material

### Bonus Target

Component (Target Behavior)->`Score Amount` = 0;
Component (Target Behavior)->`Time Amount` = 3;
Component (Target Behavior)->`Explosion Prefab` = Target-Bonus-Explosion;
Component (Target Mover)->`Motion State` = Spin;
Component (Target Exit)->`Exit After Seconds` = 5;
Material -> Target-Bonus Material

## Spawner Setup

The objective here is define the spawners and understand the script provided to spawn the targets.

### Setup

First of all, remove the game object `Targets`, used to store all targets in the game.

1. Modify the `Game Manager` to define the `Start Time` with 15 seconds;
2. Create an empty game object, reset the transform and rename to `Spawner`;
3. Insert the script the `Spawn Game Objects`, with this values:
    -  `Spawn Objects` -> `Size` = 6;
    -  `Element 0` = Target-Bonus
    -  `Element 1` = Target-Negative
    -  `Element 2` = Target-Positive1
    -  `Element 3` = Target-Positive1
    -  `Element 4` = Target-Positive2
    -  `Element 5` = Target-Positive2

### Spawn Game Objects

Explain briefly this script (can be write in portuguese)
 
## Animated Targets

Objectives here is to use the animation editor to create keyframe animations and create Spawn, Idle and Exit animations.

1. Create an animation called `Target-Spawner` and define:

At frame `0`, set the scale of target to (0,0,0);
At frame `10`, set the scale of target to (1.3,1.3,1.3);
At frame `25`, set the scale of target to (2,2,2);
Turn of the `Loop Time`.

2. Create an animation called `Target-Exit` and define:

At frame `0`, set the scale of target to (2,2,2);
At frame `10`, set the scale of target to (2.1,2.1,2.1);
At frame `25`, set the scale of target to (0,0,0);
Turn of the `Loop Time`.

Now setup the Animations:

1. Rename the Animator controller created to `Target Animator Controller`;
2. Open the `Target Animator Controller`;
3. Set the transitions Target-Spawner -> Target-Idle -> Target-Exit to apply in all targets in the game. Assume the Trigger condition to get out of Idle-State is `Exit`. Check the `TargetExit` script.

## Bonus task

Now you complete the game. If you want create a second level with more dificult and integrate the two levels (such as pass the level when reach 50 points) and another changes do you want. Remember to split the modifications using commits (one commit for a feature or modification)



